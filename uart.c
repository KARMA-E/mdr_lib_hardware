#include "uart.h"

#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <MDR32Fx.h>

#define PER_CLOCK_UART1_EN_Pos          (6)
#define PER_CLOCK_UART1_EN              ((uint32_t)(1 << PER_CLOCK_UART1_EN_Pos))
#define PER_CLOCK_UART2_EN_Pos          (7)
#define PER_CLOCK_UART2_EN              ((uint32_t)(1 << PER_CLOCK_UART2_EN_Pos))

#define UART_LCR_H_5BIT                 (0x0)
#define UART_LCR_H_6BIT                 (0x1)
#define UART_LCR_H_7BIT                 (0x2)
#define UART_LCR_H_8BIT                 (0x3)

static inline MDR_UART_TypeDef * GetUartPtr(UART_Interface_e interface)
{
    uint32_t uartBa = 0;

    switch (interface)
    {
    case UART_Interface1:
        uartBa = MDR_UART1_BASE;
        break;

    case UART_Interface2:
        uartBa = MDR_UART2_BASE;
        break;

    default:
        //TODO: add assert
        break;
    }

    return (MDR_UART_TypeDef*)uartBa;
}

static inline void UartClkEnable(UART_Interface_e interface)
{
    switch (interface)
    {
    case UART_Interface1:
        MDR_RST_CLK->PER_CLOCK |= PER_CLOCK_UART1_EN;
        MDR_RST_CLK->UART_CLOCK |= RST_CLK_UART_CLOCK_UART1_CLK_EN;
        NVIC_EnableIRQ(UART1_IRQn);
        break;

    case UART_Interface2:
        MDR_RST_CLK->PER_CLOCK |= PER_CLOCK_UART2_EN;
        MDR_RST_CLK->UART_CLOCK |= RST_CLK_UART_CLOCK_UART2_CLK_EN;
        NVIC_EnableIRQ(UART2_IRQn);
        break;

    default:
        //TODO: add assert
        break;
    }
}

bool UART_Init(UART_Interface_e interface, uint32_t baudRate, uint32_t cpuFreqHz)
{
    if((cpuFreqHz < (baudRate * 16)) || (cpuFreqHz > (baudRate * 16 * 65535)))
    {
        return false;
    }

    MDR_UART_TypeDef *uart = GetUartPtr(interface);

    uint32_t divider = cpuFreqHz / ((baudRate * 16) >> 6);
    uint32_t integerDivider = divider >> 6;
    uint32_t fractionalDivider = divider & 0x3F;

    uint32_t realBaudRate = (cpuFreqHz * ((1 << 6) / 16)) / ((integerDivider << 6) + fractionalDivider);
    uint32_t baudRateError = ((realBaudRate - baudRate) * 128) / baudRate;

    if (baudRateError > 2)
    {
        return false;
    }

    UartClkEnable(interface);

    uart->CR    &= ~UART_CR_UARTEN;
    uart->IBRD   = integerDivider;
    uart->FBRD   = fractionalDivider;
    uart->LCR_H |= UART_LCR_H_8BIT << UART_LCR_H_WLEN_Pos;
    //uart->LCR_H |= UART_LCR_H_PEN;
    //uart->LCR_H |= UART_LCR_H_EPS;
    uart->CR    |= UART_CR_UARTEN | UART_CR_TXE | UART_CR_RXE;
    uart->IMSC  |= UART_IMSC_RXIM;

    return true;
}

void UART_Send(UART_Interface_e interface, const uint8_t *buf, uint16_t len)
{
    MDR_UART_TypeDef *uart = GetUartPtr(interface);

    for(uint16_t i = 0; i < len; i++)
    {
        uart->DR = (buf[i] & (uint16_t)0x01FF);
        while(uart->FR & UART_FR_BUSY){}
    }
}

void UART_Printf(UART_Interface_e interface, const char * str, ...)
{
    va_list args;
    va_start(args, str);

    // TODO: check buf size
    uint8_t sendBuf[200];
    vsprintf((char*)sendBuf, str, args);
    UART_Send(interface, sendBuf, strlen((char*)sendBuf));

    va_end(args);
}

void UART1_IRQHandler(void)
{
    if(MDR_UART1->RIS & UART_RIS_RXRIS)
    {
        MDR_UART1->DR;
    }
    MDR_UART1->ICR |= UART_ICR_RXIC;
}


void UART2_IRQHandler(void)
{
    if(MDR_UART2->RIS & UART_RIS_RXRIS)
    {
        MDR_UART2->DR;
    }
    MDR_UART2->ICR |= UART_ICR_RXIC;
}
