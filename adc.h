#pragma once

#include <stdint.h>

void        ADC_Init(uint8_t divPwrOf2);
void        ADC_BeginConv(uint8_t channel);
void        ADC_WaitReady(void);
uint16_t    ADC_GetResult(void);

