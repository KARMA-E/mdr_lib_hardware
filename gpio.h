#pragma once

#include <stdint.h>
#include <stdbool.h>

typedef enum
{
    GPIO_PortA,
    GPIO_PortB,
    GPIO_PortC,
    GPIO_PortD,
    GPIO_PortE,
    GPIO_PortF
} GPIO_Port_e;

typedef enum
{
    GPIO_ModeIo         = 0,
    GPIO_ModeBaseFunc   = 1,
    GPIO_ModeAlterFun   = 2,
    GPIO_ModeRedefFunc  = 3,
    GPIO_ModeAnalog     = 4
} GPIO_Mode_e;

typedef enum
{
    GPIO_DsLow      = 1,
    GPIO_DsNormal   = 2,
    GPIO_DsHigh     = 3
} GPIO_Ds_e;

typedef struct
{
    GPIO_Port_e     port;
    uint8_t         pin;
    GPIO_Mode_e     mode;
    bool            output;
    bool            openDrain;
    GPIO_Ds_e       driveStrength;
} GPIO_Config_s;

void GPIO_Init(GPIO_Config_s const  *config);
void GPIO_SetState(GPIO_Port_e port, uint8_t pin, bool state);
bool GPIO_GetState(GPIO_Port_e port, uint8_t pin);
void GPIO_ToggleState(GPIO_Port_e port, uint8_t pin);
