#include "sys_clk.h"
#include <MDR32Fx.h>

#define RST_CLK_CPU_CLOCK_CPU_C1_SEL_HSI            (0U)
#define RST_CLK_CPU_CLOCK_CPU_C1_SEL_HSI_DIV_2      (1U)
#define RST_CLK_CPU_CLOCK_CPU_C1_SEL_HSE            (2U)
#define RST_CLK_CPU_CLOCK_CPU_C1_SEL_HSE_DIV_2      (3U)

#define RST_CLK_PER_CLOCK_BKP_EN_Pos                (27)
#define RST_CLK_PER_CLOCK_BKP_EN                    ((uint32_t)(1 << RST_CLK_PER_CLOCK_BKP_EN_Pos))


static volatile uint32_t _tickCnt;

uint32_t SYS_CLK_Init(SYS_CLK_Config_s const *config)
{
    MDR_RST_CLK->CPU_CLOCK &= ~RST_CLK_CPU_CLOCK_HCLK_SEL_Msk;

    uint32_t cpuC1SelVal = 0;
    uint32_t cpuC1FreqHz = 0;

    if (config->hsGenType == SYS_CLK_HsGenTypeHse)
    {
        MDR_RST_CLK->HS_CONTROL |= RST_CLK_HS_CONTROL_HSE_ON;
        while(!(MDR_RST_CLK->CLOCK_STATUS & RST_CLK_CLOCK_STATUS_HSE_RDY)) {}
        cpuC1SelVal = config->hsDiv2Enable ? RST_CLK_CPU_CLOCK_CPU_C1_SEL_HSE_DIV_2 : RST_CLK_CPU_CLOCK_CPU_C1_SEL_HSE;
        cpuC1FreqHz = config->hsDiv2Enable ? config->hseFreqHz / 2 : config->hseFreqHz;
    }
    else
    {
        cpuC1SelVal = config->hsDiv2Enable ? RST_CLK_CPU_CLOCK_CPU_C1_SEL_HSI_DIV_2 : RST_CLK_CPU_CLOCK_CPU_C1_SEL_HSI;
        cpuC1FreqHz = config->hsDiv2Enable ? SYS_CLK_HSI_FREQ_HZ / 2 : SYS_CLK_HSI_FREQ_HZ;
    }

    MDR_RST_CLK->CPU_CLOCK &= ~RST_CLK_CPU_CLOCK_CPU_C1_SEL_Msk;
    MDR_RST_CLK->CPU_CLOCK |= (uint32_t)cpuC1SelVal << RST_CLK_CPU_CLOCK_CPU_C1_SEL_Pos;
    uint32_t cpuC2FreqHz = 0;

    if (config->pllMul > 1)
    {
        MDR_RST_CLK->PLL_CONTROL |= (uint32_t)(config->pllMul - 1) << RST_CLK_PLL_CONTROL_PLL_CPU_MUL_Pos;
        MDR_RST_CLK->PLL_CONTROL |= RST_CLK_PLL_CONTROL_PLL_CPU_ON;
        while(!(MDR_RST_CLK->CLOCK_STATUS & RST_CLK_CLOCK_STATUS_PLL_CPU_RDY)) {}
        MDR_RST_CLK->CPU_CLOCK |= RST_CLK_CPU_CLOCK_CPU_C2_SEL;
        cpuC2FreqHz = cpuC1FreqHz * config->pllMul;
    }
    else
    {
        MDR_RST_CLK->CPU_CLOCK &= ~RST_CLK_CPU_CLOCK_CPU_C2_SEL;
        cpuC2FreqHz = cpuC1FreqHz;
    }

    MDR_RST_CLK->CPU_CLOCK &= ~RST_CLK_CPU_CLOCK_CPU_C3_SEL_Msk;

    if (config->divPwrOf2 > 0)
    {
        MDR_RST_CLK->CPU_CLOCK |= 0x08 << RST_CLK_CPU_CLOCK_CPU_C3_SEL_Pos;
        MDR_RST_CLK->CPU_CLOCK |= (uint32_t)(config->divPwrOf2 - 1) << RST_CLK_CPU_CLOCK_CPU_C3_SEL_Pos;
    }

    MDR_RST_CLK->CPU_CLOCK |= 0x01 << RST_CLK_CPU_CLOCK_HCLK_SEL_Pos;

    uint32_t cpuC3FreqHz = cpuC2FreqHz >> config->divPwrOf2;
    return cpuC3FreqHz;
}

void SYS_CLK_SetHsiTrim(uint32_t hsiTrimVal)
{
    MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_BKP_EN;
    MDR_BKP->REG_0F &= ~BKP_REG_0F_HSI_TRIM_Msk;
    MDR_BKP->REG_0F |= (hsiTrimVal << BKP_REG_0F_HSI_TRIM_Pos) & BKP_REG_0F_HSI_TRIM_Msk;
}

void SYS_CLK_TickInit(uint32_t periodMs, uint32_t cpuFreqHz)
{
    SysTick->LOAD = ((uint64_t)periodMs * cpuFreqHz / 1000) - 1;
    SysTick->CTRL |= SysTick_CTRL_CLKSOURCE_Msk;
    SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;
    NVIC_EnableIRQ(SysTick_IRQn);
    SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
}

uint32_t SYS_CLK_GetTick(void)
{
    return _tickCnt;
}

void SYS_CLK_DelayMs(uint32_t wait)
{
    uint32_t tickStart = _tickCnt;
    while(_tickCnt < tickStart + wait);
}

void SysTick_Handler()
{
    _tickCnt++;
}
