#include "adc.h"
#include "MDR32Fx.h"

//------------------------------------------------- ADC --------------------------------------------------

#define RST_CLK_PER_CLOCK_ADC_EN_Pos            (17)
#define RST_CLK_PER_CLOCK_ADC_EN                ((uint32_t)(1 << RST_CLK_PER_CLOCK_ADC_EN_Pos))

void ADC_Init(uint8_t divPwrOf2)
{
    MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_ADC_EN;

    MDR_ADC->ADC1_CFG |= ADC1_CFG_REG_ADON;
    MDR_ADC->ADC1_CFG |= (uint32_t)divPwrOf2 << ADC1_CFG_REG_DIVCLK_Pos;
}

void ADC_BeginConv(uint8_t channel)
{
    MDR_ADC->ADC1_CFG |= (uint32_t)channel << ADC1_CFG_REG_CHS_Pos;
    MDR_ADC->ADC1_CFG |= ADC1_CFG_REG_GO;
}

// TODO: add timeout
void ADC_WaitReady(void)
{
    while((MDR_ADC->ADC1_STATUS & ADC_STATUS_FLG_REG_EOCIF) != ADC_STATUS_FLG_REG_EOCIF) {}
}

uint16_t ADC_GetResult(void)
{
    return (uint16_t) MDR_ADC->ADC1_RESULT;
}










