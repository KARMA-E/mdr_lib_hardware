#pragma once

#include <stdint.h>
#include <stdbool.h>

typedef enum
{
    UART_Interface1 = 1,
    UART_Interface2 = 2
} UART_Interface_e;

bool UART_Init(UART_Interface_e interface, uint32_t baudRate, uint32_t cpuFreqHz);
void UART_Send(UART_Interface_e interface, const uint8_t *buf, uint16_t len);
void UART_Printf(UART_Interface_e interface, const char *str, ...);
