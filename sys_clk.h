#pragma once

#include <stdint.h>
#include <stdbool.h>

#define SYS_CLK_HSI_FREQ_HZ         (8##000##000)

typedef enum
{
    SYS_CLK_HsGenTypeHsi,
    SYS_CLK_HsGenTypeHse
}SYS_CLK_HsGenType_e;

typedef struct
{
    SYS_CLK_HsGenType_e hsGenType;
    bool                hsDiv2Enable;
    uint32_t            hseFreqHz;
    uint32_t            pllMul;
    uint32_t            divPwrOf2;
}SYS_CLK_Config_s;

uint32_t    SYS_CLK_Init(SYS_CLK_Config_s const *config);
void        SYS_CLK_SetHsiTrim(uint32_t hsiTrimVal);
void        SYS_CLK_TickInit(uint32_t periodMs, uint32_t cpuFreqHz);
uint32_t    SYS_CLK_GetTick(void);
void        SYS_CLK_DelayMs(uint32_t wait);
