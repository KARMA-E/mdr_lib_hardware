#include "gpio.h"
#include <MDR32Fx.h>

#define RST_CLK_PER_CLOCK_PORTA_EN_Pos          (21)
#define RST_CLK_PER_CLOCK_PORTA_EN              ((uint32_t)(1 << RST_CLK_PER_CLOCK_PORTA_EN_Pos))
#define RST_CLK_PER_CLOCK_PORTB_EN_Pos          (22)
#define RST_CLK_PER_CLOCK_PORTB_EN              ((uint32_t)(1 << RST_CLK_PER_CLOCK_PORTB_EN_Pos))
#define RST_CLK_PER_CLOCK_PORTC_EN_Pos          (23)
#define RST_CLK_PER_CLOCK_PORTC_EN              ((uint32_t)(1 << RST_CLK_PER_CLOCK_PORTC_EN_Pos))
#define RST_CLK_PER_CLOCK_PORTD_EN_Pos          (24)
#define RST_CLK_PER_CLOCK_PORTD_EN              ((uint32_t)(1 << RST_CLK_PER_CLOCK_PORTD_EN_Pos))
#define RST_CLK_PER_CLOCK_PORTE_EN_Pos          (25)
#define RST_CLK_PER_CLOCK_PORTE_EN              ((uint32_t)(1 << RST_CLK_PER_CLOCK_PORTE_EN_Pos))
#define RST_CLK_PER_CLOCK_PORTF_EN_Pos          (29)
#define RST_CLK_PER_CLOCK_PORTF_EN              ((uint32_t)(1 << RST_CLK_PER_CLOCK_PORTF_EN_Pos))

static inline MDR_PORT_TypeDef * GetPortPtr(GPIO_Port_e port)
{
    uint32_t portBa = 0;

    switch (port)
    {
    case GPIO_PortA:
        portBa = MDR_PORTA_BASE;
        break;

    case GPIO_PortB:
        portBa = MDR_PORTB_BASE;
        break;

    case GPIO_PortC:
        portBa = MDR_PORTC_BASE;
        break;

    case GPIO_PortD:
        portBa = MDR_PORTD_BASE;
        break;

    case GPIO_PortE:
        portBa = MDR_PORTE_BASE;
        break;

    case GPIO_PortF:
        portBa = MDR_PORTF_BASE;
        break;

    default:
        //TODO: add assert
        break;
    }

    return (MDR_PORT_TypeDef*)portBa;
}

static inline void PortClockEnable(GPIO_Port_e port)
{
    switch (port)
    {
    case GPIO_PortA:
        MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PORTA_EN;
        break;

    case GPIO_PortB:
        MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PORTB_EN;
        break;

    case GPIO_PortC:
        MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PORTC_EN;
        break;

    case GPIO_PortD:
        MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PORTD_EN;
        break;

    case GPIO_PortE:
        MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PORTE_EN;
        break;

    case GPIO_PortF:
        MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PORTF_EN;
        break;

    default:
        //TODO: add assert
        break;
    }
}

void GPIO_Init(GPIO_Config_s const *config)
{
    PortClockEnable(config->port);
    MDR_PORT_TypeDef *mdrPort = GetPortPtr(config->port);
    const uint32_t pinMsk = 1 << config->pin;

    mdrPort->FUNC &= ~(PORT_FUNC_MODE0_Msk << (2 * config->pin));

    if (config->mode == GPIO_ModeAnalog)
    {
        mdrPort->ANALOG &= ~pinMsk;
    }
    else
    {
        mdrPort->ANALOG |= pinMsk;
        mdrPort->FUNC |= (PORT_FUNC_MODE0_Msk & config->mode) << (2 * config->pin);
    }

    if (config->output)
    {
        mdrPort->PD = (config->openDrain) ? (mdrPort->PD | pinMsk)
                                          : (mdrPort->PD & ~pinMsk);

        mdrPort->PWR &= ~(PORT_PWR0_Msk << (2 * config->pin));
        mdrPort->PWR |=  (PORT_PWR0_Msk & config->driveStrength) << (2 * config->pin);

        mdrPort->OE |= pinMsk;
    }
    else
    {
        mdrPort->OE &= ~pinMsk;
    }
}

void GPIO_SetState(GPIO_Port_e port, uint8_t pin, bool state)
{
    MDR_PORT_TypeDef *mdrPort = GetPortPtr(port);
    const uint32_t pinMsk = 1 << pin;

    mdrPort->RXTX = (state) ? (mdrPort->RXTX | pinMsk)
                            : (mdrPort->RXTX & ~pinMsk);
}

bool GPIO_GetState(GPIO_Port_e port, uint8_t pin)
{
    MDR_PORT_TypeDef *mdrPort = GetPortPtr(port);
    const uint32_t pinMsk = 1 << pin;
    return (mdrPort->RXTX & pinMsk) == pinMsk;
}

void GPIO_ToggleState(GPIO_Port_e port, uint8_t pin)
{
    MDR_PORT_TypeDef *mdrPort = GetPortPtr(port);
    mdrPort->RXTX ^= 1 << pin;
}
